<?php

namespace App\Http\Controllers\Back\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use DB;

class AuthController extends Controller
{
    
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
        $this->middleware('guest:admin')->except('logout');
    }

    public function formLoginAdmin()
    {
        return view('back.auth.login', ['url' => 'admin']);
    }

    public function storeLoginAdmin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        $cekUser = DB::table('users')->where('email', $request->email)->where('flag', 1)->first();
        if ($cekUser) {    
            if (Auth::guard('admin')->attempt(
                [
                    'email' => $request->email, 
                    'password' => $request->password
                ], 
                $request->get('remember')
            )) {
                return redirect()->intended('/admin/dashboard');
            }
        }else{
            toastr()->error('Login gagal, akun anda tidak memiliki akses buyer/non-aktif.');
            return redirect()->back();        
        }
        toastr()->error('Username/Password salah.');    
        return back()->withInput($request->only('email', 'remember'));
    }
}