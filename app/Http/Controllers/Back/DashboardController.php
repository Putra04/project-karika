<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use DataTables;
use Auth;
use DB;

class DashboardController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth:admin');
  }
  
  public function index(Request $request)
  {

    return view('back.dashboard.index');
}
}