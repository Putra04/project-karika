<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// FRONT
Route::get('/', [App\Http\Controllers\Front\HomeController::class, 'index'])->name('index');

Auth::routes();
Route::group(['prefix' => 'admin', 'middleware', 'guest:admin'], function () {
  Route::get('/login', [App\Http\Controllers\Back\Auth\AuthController::class, 'formLoginAdmin'])->name('login.admin.form');
  Route::post('/register/auth',  [App\Http\Controllers\Back\Auth\AuthController::class, 'storeLoginAdmin'])->name('login.admin.store');
  Route::get('/logout', [App\Http\Controllers\Back\Auth\AuthController::class, 'logout'])->name('login.admin.logout');

  Route::group(['middleware', 'auth:admin'], function () {
    Route::group(['prefix' => '/dashboard'], function () {
      Route::get('/', [App\Http\Controllers\Back\DashboardController::class, 'index'])->name('admin.dashboard');
    });
  });
});
