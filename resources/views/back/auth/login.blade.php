<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->

<head>
    <base href="../../../" />
    <title>Login</title>

    <!--begin::Fonts(mandatory for all pages)-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
    <link href="back/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="back/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
    <script>
    // Frame-busting to prevent site from being loaded within a frame without permission (click-jacking) if (window.top != window.self) { window.top.location.replace(window.self.location.href); }
    </script>
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_body" class="auth-bg bgi-size-cover bgi-attachment-fixed bgi-position-center bgi-no-repeat">
    <!--begin::Theme mode setup on page load-->
    <script>
    var defaultThemeMode = "light";
    var themeMode;
    if (document.documentElement) {
        if (document.documentElement.hasAttribute("data-bs-theme-mode")) {
            themeMode = document.documentElement.getAttribute("data-bs-theme-mode");
        } else {
            if (localStorage.getItem("data-bs-theme") !== null) {
                themeMode = localStorage.getItem("data-bs-theme");
            } else {
                themeMode = defaultThemeMode;
            }
        }
        if (themeMode === "system") {
            themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
        }
        document.documentElement.setAttribute("data-bs-theme", themeMode);
    }
    </script>



    <div class="d-flex flex-column flex-root">

        <style>
        body {
            background-image: url('back/assets/media/auth/bg4.jpg');
        }

        [data-bs-theme="dark"] body {
            background-image: url('back/assets/media/auth/bg4-dark.jpg');
        }
        </style>


        <div class="d-flex flex-column flex-column-fluid flex-lg-row">
            <div class="d-flex flex-center w-lg-50 pt-15 pt-lg-0 px-10">
                <div class="d-flex flex-center flex-lg-start flex-column">
                    <a href="../../demo9/dist/index.html" class="mb-7">
                        <img alt="Logo" src="back/assets/media/logos/banner-hero.svg" />
                    </a>
                    <h2 class="text-white fw-normal m-0">Because in your dreams, every detail matters.</h2>
                </div>
            </div>

            <div
                class="d-flex flex-column-fluid flex-lg-row-auto justify-content-center justify-content-lg-end p-12 p-lg-20">
                <div class="bg-body d-flex flex-column align-items-stretch flex-center rounded-4 w-md-600px p-20">
                    <div class="d-flex flex-center flex-column flex-column-fluid px-lg-10 pb-15 pb-lg-20">
                        <form method="POST" class="form w-100" novalidate="novalidate" id="kt_sign_in_form"
                            action="{{route('login.admin.store')}}">
                            @csrf

                            <div class="text-center mb-11">
                                <h1 class="text-dark fw-bolder mb-3">Sign In</h1>
                                <div class="text-gray-500 fw-semibold fs-6">Your Social Campaigns</div>
                            </div>
                            <div class="row g-3 mb-9">
                            </div>
                            <div class="fv-row mb-8">

                                <input required autofocus type="text" id="email" name="email" placeholder="Email"
                                    autocomplete="email" value="{{ old('email') }}"
                                    class="form-control bg-transparent @error('email') is-invalid @enderror" />

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message ?? '' }}</strong>
                                </span>
                                @enderror

                            </div>

                            <div class="fv-row mb-3">

                                <input required id="password" type="password" name="password" placeholder="Password"
                                    autocomplete="current-password"
                                    class="form-control bg-transparent @error('password') is-invalid @enderror" />

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message ?? '' }}</strong>
                                </span>
                                @enderror

                            </div>



                            <div class="d-grid mb-10">
                                <button type="submit" class="btn btn-primary">
                                    <span class="indicator-label">Masuk</span>
                                    <span class="indicator-progress">Please wait...
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                </button>
                            </div>



                        </form>

                    </div>


                </div>

            </div>

        </div>

    </div>



    <script>
    var hostUrl = "assets/";
    </script>

    <script src="back/assets/plugins/global/plugins.bundle.js"></script>
    <script src="back/assets/js/scripts.bundle.js"></script>


    <script src="back/assets/js/custom/authentication/sign-in/general.js"></script>


</body>

</html>